

/* Find users with the letter “s” in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field.
*/

db.users.find(
		{
			$or: [
				{firstName: {
					$regex: 's',
					$options: '$i'}
				},
				{lastName: {
					$regex: 'd',
					$options: '$i'}
				},
			]
		},
			{
				firstName: 1,
				lastName: 1,
				_id: 0
			}
		
);



/*
Find users who are from the HR department and their age is greater than or equal to 70.
a. Use the $and operator
*/

db.users.find(
	{
		$and: [
			{age: {$gte: 70}},
			{department: "HR"}
			]
	}
);



/*
Find users with the letter e in their first name and have an age of less than or equal to 30.
a. Use the $and, $regex and $lte operators
*/

db.users.find(
		{
			$and: [
				{firstName: {
					$regex: "e",
					$options: "$i"}
				},
				{age: 
					{$lte: 30}
				}
			]
		}
);